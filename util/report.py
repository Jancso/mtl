#!/usr/bin/env python3
# coding: utf8


"""
Create a report for a training or prediction run.
"""



import io
import time
import logging

from .misc import smart_open, get_commit_info


TEMPLATE = '''\
# Commit hash:

{}


# Commit message:

{}


# Execution timestamp:

{}


# Results:

{}


# Log:

{}


# Configuration:

{}
'''


RESULT_TMPL = '''\
F1\t{fscore}
P\t{prec}
R\t{rec}
TP\t{tp}
FP\t{fp}
FN\t{fn}
'''


class Reporter:
    '''
    Collect and format information about results and config.
    '''

    def __init__(self, conf):
        self.conf = conf
        self.commit_hash = get_commit_info('H', 'hash')
        self.commit_msg = get_commit_info('B', 'message')
        self.timestamp = self._reformat_timestamp(conf.timestamp)
        self.log = self._capture_log()
        self.results = io.StringIO()

    @staticmethod
    def _reformat_timestamp(timestamp):
        return time.strftime('%Y-%m-%d %H:%M:%S',
                             time.strptime(timestamp, '%Y%m%d-%H%M%S'))

    def _capture_log(self):
        logger = logging.getLogger()  # root logger
        stream = io.StringIO()
        handler = logging.StreamHandler(stream)
        handler.setLevel(logging.INFO)
        handler.setFormatter(logging.Formatter(self.conf.logging_format))
        logger.addHandler(handler)
        return stream

    def add_results(self, overall, by_type=None, replace=True):
        """
        Read results from a conlleval.Metrics namedtuple.
        """
        if replace:  # remove any previously written results
            self.results.seek(0)
            self.results.truncate()

        self._add_results(overall)
        if by_type:
            for type_, results in by_type.items():
                self.results.write('\n{}\n'.format(type_))
                self._add_results(results)

    def _add_results(self, results):
        self.results.write(RESULT_TMPL.format(**results._asdict()))

    def dump(self):
        '''
        Write a formatted record.
        '''
        destination = self.conf.report_path
        with smart_open(destination, 'w') as f:
            f.write(TEMPLATE.format(self.commit_hash,
                                    self.commit_msg,
                                    self.timestamp,
                                    self.results.getvalue(),
                                    self.log.getvalue(),
                                    self.conf.dump()))
