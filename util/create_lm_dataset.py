import logging
from config import Config as Cfg
from util.data_utils import iter_words_labels_from_conll_files, START


def create_language_modeling_dataset(output_path, *input_paths):
    """Create a language modeling dataset.

    The dataset is created from Named Entity Recognition (NER) CoNLL files and
    written back to a file in the CoNLL format:

    word1\tword2
    word2\tword3
    word3\tword4
    ...

    Args:
        output_path (Path): Path of the language modeling dataset.
        *input_paths (Path): Paths to NER CoNLL files.
    """
    logging.info('Creating language modeling dataset...')
    with open(output_path, 'w') as f:
        word_iterator = iter_words_labels_from_conll_files(*input_paths)

        # first word
        word, _ = next(word_iterator)

        for pred_word, _ in word_iterator:

            f.write(word + '\t' + pred_word + '\n')

            if pred_word == START:
                f.write('\n')

            word = pred_word


def main(cfg):
    """Create LM out of all NER datasets specified in the config `cfg`."""
    logging.basicConfig(format=cfg.logging_format,
                        level=cfg.logging_level)

    output_base_path = cfg.data_path / 'LM'
    output_base_path.mkdir(exist_ok=True, parents=True)

    for pos, name in [(0, 'train.tsv'), (1, 'dev.tsv'), (2, 'test.tsv')]:
        inputs = [cfg.datasets_paths['NER'][dataset][pos]
                  for dataset in cfg.datasets_paths['NER']]
        output = output_base_path / name

        create_language_modeling_dataset(output, *inputs)


if __name__ == '__main__':
    main(Cfg)
