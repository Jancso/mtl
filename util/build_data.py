#!/usr/bin/env python3
# coding: utf8
"""Data build: prepare vocabularies and word embeddings."""

import sys
import shutil

from config import Config
from util.data_utils import *


def build(config):
    """Build the data.

    Only performs a data build if no previous build can be found.

    Args:
        config (Config): The config instance.
    """
    # Check for existing build
    if config.data_path.is_dir():
        logging.info('Found existing data build at %s', config.data_path)
    else:
        try:
            logging.info('Building data under %s ...', config.data_path)
            config.data_path.mkdir(parents=True)
            _build(config)
        except Exception:
            shutil.rmtree(config.data_path)
            raise


def _build(config):
    """Build the data.

    Creates the following files:
    - dataset-specific data in `{data_path}/{dataset}/`
        - label vocabulary in `label_vocab.txt`
    - global char vocabulary in `{data_path}/char_vocab.txt`
    - global word vocabulary in `{data_path}/word_vocab.txt`
    - compressed word embeddings in `{data_path}/wembs.npz`

    Args:
        config (Config): The config instance.
    """
    data_path = config.data_path

    # global word and char vocabularies
    word_vocab = set()
    char_vocab = set()

    datasets_paths = config.datasets_paths

    for task_type in datasets_paths:
        for dataset in datasets_paths[task_type]:

            # get base path for dataset directory and create it
            dataset_path = data_path / dataset
            dataset_path.mkdir()

            # get paths to train, dev and test set
            train_path, dev_path, test_path = \
                datasets_paths[task_type][dataset]

            # get word, char and label vocabularies from train set
            words, chars, labels = get_vocabs_from_conll_files(train_path)

            # add word & char vocabularies of train set to global vocabularies
            word_vocab.update(words)
            char_vocab.update(chars)

            # if there are pre-trained word embeddings
            if config.use_pretrained_wembs:
                # also add word & char vocabularies from dev and test set
                # to include embeddings of words not occurring in train set
                words, chars, _ = get_vocabs_from_conll_files(dev_path,
                                                              test_path)
                word_vocab.update(words)
                char_vocab.update(chars)

            # write label vocabulary to a file
            write_vocab_to_file(labels, dataset_path / 'label_vocab.txt')

    if config.use_pretrained_wembs:
        # get pre-trained word embeddings
        wembs = get_raw_embeddings_from_file(config.raw_wembs_path)
        # take intersection of dataset and embeddings vocabularies
        # to speed up processing
        word_vocab.intersection_update(wembs.vocab)
    else:
        wembs = None

    # add special tokens
    char_vocab.add(UNK_CHAR)
    word_vocab.add(UNK_WORD)
    word_vocab.add(NUM)
    word_vocab.add(START)
    word_vocab.add(END)

    # write char vocabulary to a file
    write_vocab_to_file(char_vocab, data_path / 'char_vocab.txt')

    # write word vocabulary to a file
    write_vocab_to_file(word_vocab, data_path / 'word_vocab.txt')

    if wembs:
        # write embeddings to a compressed file
        write_embeddings_to_npz_file(
            word_vocab, wembs, data_path / 'wembs.npz')


def main():
    """Entry point from command-line."""
    config = Config(*sys.argv[1:])
    build(config)


if __name__ == '__main__':
    main()
