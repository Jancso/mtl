#!/usr/bin/env python3
# coding: utf8


"""
A stack of multiple progress bars.
"""


import os
import sys
import logging

try:
    from blessings import Terminal
    from progressbar import ProgressBar, Bar
except ImportError:
    logging.warning('progress bars disabled due to missing dependencies '
                    '(blessings, progressbar)')
    Terminal, ProgressBar, Bar = None, None, None


BARWIDTH = 50


class ProgressBarGroup:
    """
    A group of progress bars.
    """
    def __init__(self):
        self.term = Terminal()
        self.pbars = []
        self.label_width = 0
        self.maxvalue = 0

    def add(self, label, maxvalue, info):
        """Create a new progressbar."""
        pbar = ProgressBarMember(maxvalue, info, self.term)
        self.pbars.append(pbar)
        self._adjust_all(label, maxvalue)
        return pbar

    def _adjust_all(self, label, maxvalue):
        """Adjust x and y anchors of all bars and print a label."""
        print(label)  # add a line for this bar
        self.label_width = max(self.label_width, len(label) + 1)
        self.maxvalue = max(self.maxvalue, maxvalue)
        barwidth_norm = (BARWIDTH-2) / self.maxvalue
        for pbar in self.pbars:
            pbar.x = self.label_width
            pbar.y -= 1  # move up one line
            pbar.pbar.term_width = round(pbar.pbar.maxval * barwidth_norm) + 2

    def finish(self):
        """Set all bars to finished state."""
        for pbar in self.pbars:
            pbar.finish()


class ProgressBarMember:
    """
    A single progress bar in a group of bars.
    """
    def __init__(self, maxvalue, info, term):
        self.term = term
        self.info_tmpl = info
        self.x = 0
        self.y = self.term.height - 1
        self.widget = Bar(left='[', right=']', fill='.')
        self.pbar = ProgressBar(maxval=maxvalue,
                                widgets=[self.widget],
                                term_width=BARWIDTH,
                                fd=self)
        self.value = None

    def start(self):
        """Initialise value."""
        self.value = 0
        self.pbar.start()
        return self  # imitate progressbar.ProgressBar.start()

    def update(self, *info):
        """Wrap updating."""
        if self.value is None:
            self.start()
        self.value += 1
        self.pbar.update(self.value)
        info = self.info_tmpl.format(*info)
        self.write(info, self.x + BARWIDTH + 1, fill=True)

    def write(self, text, x=None, fill=False):
        """Print at the right position."""
        if x is None:
            x = self.x
        if fill:
            text = text.ljust(self.term.width-x)
        with self.term.location(x, self.y):
            print(text)

    def flush(self):
        """No-op."""

    def finish(self):
        """Catch cases where bars are unfinished."""
        currval, maxval = self.pbar.currval, self.pbar.maxval
        if currval < maxval:
            new_width = round((self.pbar.term_width-2) * currval / maxval) + 2
            self.pbar.term_width = new_width
            self.widget.right = '|'
        self.pbar.finish()


# Dummies for disabling.

class ProgressBarGroupDummy:
    """Dummy with ProgressBarGroup interface."""
    def __init__(self):
        self.pbars = []

    def add(self, *_):
        """Create and return a ProgressBarMemberDummy."""
        pbar = ProgressBarMemberDummy()
        self.pbars.append(pbar)
        return pbar

    def finish(self):
        """No-op."""


class ProgressBarMemberDummy:
    """Dummy with basic ProgressBarMember interface."""
    def start(self):
        """No-op."""
        return self

    def update(self, _):
        """No-op."""

    def finish(self):
        """No-op."""


def tty_out():
    """Check if STDOUT is connected to a TTY."""
    return hasattr(sys.stdout, 'fileno') and os.isatty(sys.stdout.fileno())


# Disable on import errors.
if Terminal is None or not tty_out():
    ProgressBarGroup = ProgressBarGroupDummy
    ProgressBarMember = ProgressBarMemberDummy
