#!/usr/bin/env python3
# coding: utf8


'''
Miscellaneous utilities.
'''


import io
import os
import sys
import gzip
import subprocess as sp


def smart_open(path, mode='r', **kwargs):
    '''
    Try to be clever at opening a file.
    '''
    if 'b' not in mode:
        kwargs.setdefault('encoding', 'utf8')

    # Special cases: memory buffer, STD channels.
    if path is None:
        return io.BytesIO() if 'b' in mode else io.StringIO()
    if str(path) == '-':
        return std(mode)

    # For actual on-disk files, create missing parent dirs.
    if 'w' in mode:
        path.parent.mkdir(parents=True, exist_ok=True)

    if str(path).endswith('.gz'):
        if 'b' not in mode and 't' not in mode:
            # If text/binary isn't specified, default to text mode.
            mode += 't'
        return gzip.open(path, mode, **kwargs)
    return open(path, mode, **kwargs)


def std(mode):
    '''
    Get STDIN/OUT as a text or binary stream.
    '''
    if 'w' in mode:
        f = sys.stdout
    else:
        f = sys.stdin
    if 'b' in mode:
        f = f.buffer
    return f


def get_commit_info(spec, fallback):
    '''
    Get some info about the current git commit.
    '''
    args = ['git', 'log', '-1', '--pretty=%{}'.format(spec)]
    compl = sp.run(args, stdout=sp.PIPE, cwd=os.path.dirname(__file__))
    if compl.returncode == 0:
        return compl.stdout.decode('utf8').strip()
    return '<no commit {}>'.format(fallback)
