import math
import random
import logging

import numpy as np
from gensim.models.keyedvectors import KeyedVectors

# special tokens
UNK_CHAR = ''  # character not in vocabulary
UNK_WORD = '$unknown_word$'  # word not in vocabulary
NUM = '$numeric_word$'  # word is a digit
START = '$start_of_sentence$'  # word for start of sentence
END = '$end_of_sentence$'  # word for end of sentence
OUT = 'O'  # out label


def iter_words_labels_from_conll_files(*paths):
    """Iter words and labels of CoNLL files.

    Args:
        *paths: Paths to CoNLL files.

    Yields:
        Tuple[str, str]: (word, label)
    """
    for path in paths:
        with open(path) as f:
            logging.info('Reading {}...'.format(path))

            yield START, OUT

            for line in f:
                line = line.strip()

                if line:
                    yield line.split('\t')
                else:
                    yield END, OUT
                    yield START, OUT


def get_vocabs_from_conll_files(*paths):
    """Get word, character and label vocabularies from CoNLL files.

    Args:
        *paths: Paths to CoNLL files.

    Returns:
        Tuple[Set[str], Set[str], Set[str]]: (words, chars, labels).
    """
    logging.info('Building word, character and label vocabularies...')
    word_vocab = set()
    char_vocab = set()
    label_vocab = set()

    for word, label in iter_words_labels_from_conll_files(*paths):
        word_vocab.add(normalize(word))
        char_vocab.update(word)
        label_vocab.add(label)

    return word_vocab, char_vocab, label_vocab


def get_data_from_conll_files(*path, ignore_special_tokens=False):
    """Get the data from CoNLL files.

    Args:
        *path: Paths to the CoNLL files.
        ignore_special_tokens (bool): `START` and `END` are not inserted.

    Returns:
        List[List[Tuple[str, str]]]: Sentences comprising word and label pairs:
        [[(w1, l1), (w2, l2), ...], ...].
    """
    data = []
    sentence = []
    for word, label in iter_words_labels_from_conll_files(*path):
        if word == START:
            sentence = []
        elif word == END:
            data.append(sentence)

        if not ignore_special_tokens:
            sentence.append((word, label))
        else:
            if word != START or label != OUT and word != END or label != OUT:
                sentence.append((word, label))

    return data


def write_vocab_to_file(vocab, path):
    """Write vocabulary to file.

    Writes one word per line in lexicographical order.

    Args:
        vocab (Set[str]): Vocabulary.
        path (Path): Path to the vocabulary file.
    """
    with open(path, 'w') as f:
        for i, token in enumerate(sorted(vocab)):
            # if not last word
            if i != len(vocab) - 1:
                f.write('{}\n'.format(token))
            else:
                f.write(token)


def get_vocab_from_file(path):
    """Get vocabulary from file.

    Args:
        path (Path): Path to the vocabulary file.

    Returns:
        Set[str]: Vocabulary.
    """
    vocab = set()
    with open(path) as f:
        for token in f:
            token = token.rstrip('\n')
            vocab.add(token)

    return vocab


def get_raw_embeddings_from_file(path):
    """Get pre-trained word embeddings from a file.

    Args:
        path (Path): Path to the serialised embeddings.

    Returns:
        KeyedVectors: gensim container for embeddings.
    """
    logging.info('Processing word embeddings...')

    # gensim does not support pathlib.Path yet
    path = str(path)

    if path.endswith('.kv'):
        wembs = KeyedVectors.load(path, mmap='r')
    else:
        wembs = KeyedVectors.load_word2vec_format(
            path, binary=path.endswith('.bin'))

    return wembs


def write_embeddings_to_npz_file(vocab, gensim_wembs, path):
    """Write word embeddings to a compressed NPZ file.

    Word embeddings are placed into an numpy array of shape (vocab size,
    embedding size) based on lexicographical order of the words. The array is
    stored in a file in compressed NPZ format.

    Args:
        vocab (Set[str]): Vocabulary.
        gensim_wembs (KeyedVectors): gensim container for embeddings.
        path (Path): Path to NPZ file.
    """
    # get array for word embeddings
    # shape = (vocab size, embedding size)
    vocab_dim = len(vocab) + 1  # +1: 0 reserved for padding
    wemb_dim = gensim_wembs.vectors.shape[1]
    np_wembs = np.zeros([vocab_dim, wemb_dim])

    for i, word in enumerate(sorted(vocab), start=1):
        # NB: special tokens are left all zeros
        # TODO: use different embeddings for special tokens?
        if word in gensim_wembs.vocab:
            wemb = gensim_wembs[word]
            np_wembs[i] = wemb

    # save array in compressed NPZ format
    np.savez_compressed(path, embeddings=np_wembs)


def get_embeddings_from_npz_file(path):
    """Get word embeddings from a compressed NPZ file.

    Args:
        path (Path): Path to the NPZ file.

    Returns:
        numpy.array: Embeddings.
    """
    with np.load(path) as data:
        return data['embeddings']


def normalize(word):
    """Normalize word.

    Normalizing steps:
    - lowercasing word
    - mapping digit word to `NUM`.

    Args:
        word (str): Word to be normalized.
    """
    # don't normalize special tokens
    if word in {UNK_WORD, NUM, START, END}:
        return word

    word = word.lower()

    if word.isdigit():
        word = NUM

    return word


def get_lexicographical_id_mapping(vocab):
    """Get a lexicographical-based ID mapping of a vocabulary.

    The items in a vocabulary are first sorted lexicographically and then
    enumerated. The position of the item in the enumeration will be the ID.
    Enumeration starts at 1 since 0 is reserved for padding.

    Args:
        vocab (Set[str]): The vocabulary.

    Returns:
        Dict[str, int]:  Mapping of vocabulary items to IDs.
    """
    mapping = {}
    for i, token in enumerate(sorted(vocab), start=1):
        mapping[token] = i

    return mapping


def word2wid(word_vocab):
    """Get a callable mapping a given word to its ID.

    The IDs are assigned based on lexicographical order. Words which do not
    appear in the vocabulary are given the ID of `UNK`. Before mapping,
    all words are normalized (see `normalize()`).

    Args:
        word_vocab (Set[str]): The word vocabulary.

    Returns:
        Callable[[str], int]: Maps word to its ID.
    """
    mapping = get_lexicographical_id_mapping(word_vocab)

    def f(word):
        word = normalize(word)

        if word in mapping:
            return mapping[word]
        else:
            return mapping[UNK_WORD]

    return f


def char2cid(char_vocab):
    """Get a callable mapping a given character to its ID.

    The IDs are assigned based on lexicographical order. Characters which do
    not appear in the vocabulary are given the ID of `UNK`.

    Args:
        char_vocab (Set[str]): The character vocabulary.

    Returns:
        Callable[[str], List[int]]: Gets character IDs of a word.
    """
    mapping = get_lexicographical_id_mapping(char_vocab)

    def f(char):
        if char in mapping:
            return mapping[char]
        else:
            return mapping[UNK_CHAR]

    return f


def label2lid(label_vocab):
    """Get function mapping a given label to its ID.

    The IDs are assigned based on lexicographical order.

    Args:
        label_vocab (Set[str]): The label vocabulary.

    Returns:
        Callable[[str], int]: Maps label to its ID.
    """
    mapping = get_lexicographical_id_mapping(label_vocab)

    def f(label):
        return mapping[label]

    return f


def iter_in_minibatches(data, size):
    """Iter data in mini-batches.

    Args:
        data (List[Any]): The whole batch, i.e. all sentences in the data.
        size (int): Size of mini-batch.

    Yields:
        List[Any]: A mini-batch, i.e a subset of all sentences of size `size`.
    """
    mini_batch = []
    for sent in data:

        if len(mini_batch) == size:
            yield mini_batch
            mini_batch = []
        else:
            mini_batch.append(sent)


def extract_data(data, char2cid, word2wid, label2lid):
    """Extract, map and pad the characters, words and labels from the data.

    Args:
        data (List[List[Tuple[str, str]]]):
            The data as returned by `get_data_from_conll_files`.
        char2cid (Callable[[str], int]): Maps char to its ID.
        word2wid (Callable[[str], int]): Maps word to its ID.
        label2lid (Callable[[str], int]): Maps label to its ID.

    Returns:
        Tuple(numpy.ndarray, numpy.ndarray, numpy.ndarray,
            numpy.ndarray, numpy.ndarray): (cids, wids, lids, slens, wlens)
    """
    # batch size
    batch_size = len(data)
    # max number of words in batch: sentence with most words in it
    max_sent_len = max(len(sent) for sent in data)
    # max number of chars in batch: word with most chars in it
    max_word_len = max(len(word) for sent in data for word, _ in sent)

    cids = np.zeros(shape=(batch_size, max_sent_len, max_word_len), dtype=int)
    wids = np.zeros(shape=(batch_size, max_sent_len), dtype=int)
    lids = np.zeros(shape=(batch_size, max_sent_len), dtype=int)
    slens = np.zeros(shape=(batch_size,), dtype=int)
    wlens = np.zeros(shape=(batch_size, max_sent_len), dtype=int)

    for sent_pos, sent in enumerate(data):
        slens[sent_pos] = len(sent)

        for word_pos, (word, label) in enumerate(sent):
            wlens[sent_pos, word_pos] = len(word)
            wids[sent_pos, word_pos] = word2wid(word)
            lids[sent_pos, word_pos] = label2lid(label)

            for char_pos, char in enumerate(word):
                cids[sent_pos, word_pos, char_pos] = char2cid(char)

    return cids, wids, lids, slens, wlens


class ShuffledSampler:
    """Handler for shuffling and sampling batches."""

    def __init__(self, datasets, batch_size, shrink_to_smallest=False):
        """Initialize variables.

        Args:
            datasets (List[List[Any]]): The datasets.
            batch_size (int): The number of sentences in a mini-batch.
            shrink_to_smallest (bool): Stop sampling when the first dataset is
                exhausted. This means all datasets are down-sampled to the size
                of the smallest dataset.
        """
        self.datasets = datasets
        self.batch_size = batch_size
        if shrink_to_smallest:
            self._choose = self._uniform_choice
        else:
            self._choose = self._weighted_choice

    def __iter__(self):
        """Iterate over pairs <i, batch>.

        The `mini-batch` is a nested structure, as returned by
        `iter_in_minibatches()`. The index `i` points to the dataset from
        which this batch was sampled.

        Note:
            Don't interleave calls to __iter__(). If __iter__() is called a
            second time before the first iterator is exhausted, then the
            two iterators will sample from the same permutation, and the first
            one is potentially messed up.
        """
        self._shuffle()
        remaining = self.nbatches()
        batchers = [iter_in_minibatches(d, self.batch_size)
                    for d in self.datasets]

        try:
            while True:
                i = self._choose(remaining)
                yield i, next(batchers[i])
                remaining[i] -= 1
        except StopIteration:
            return

    def nbatches(self):
        """Create a list with the size (#batches) of all datasets."""
        return [int(math.ceil(len(d)/self.batch_size)) for d in self.datasets]

    def _shuffle(self):
        for d in self.datasets:
            random.shuffle(d)

    @staticmethod
    def _uniform_choice(population):
        return random.randrange(len(population))

    @staticmethod
    def _weighted_choice(population):
        try:
            x = random.randrange(sum(population))
        except ValueError:
            raise StopIteration
        for i, n in enumerate(population):
            if x < n:
                return i
            x -= n
        # This point can't be reached if population has non-negative ints only.
        raise ValueError('invalid population counts')
