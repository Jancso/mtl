import time
import hashlib
from pathlib import Path


class Config:

    # *** General Settings ***

    # logging
    logging_format = '%(asctime)s - %(message)s'
    logging_level = 'INFO'

    # *** Paths ***

    # directory where all data is dumped from a data build
    data_path = Path('data')

    model_path = data_path / 'model' / 'weights'

    reports_path = data_path / 'reports'

    raw_wembs_path = data_path / 'PubMed-shuffle-win-30.bin'

    datasets_base_path = data_path / 'MTL-Bioinformatics-2016/data'

    datasets_paths = {
        'NER': {
            'AnatEM': (
                datasets_base_path / 'AnatEM-IOBES/train.tsv',
                datasets_base_path / 'AnatEM-IOBES/devel.tsv',
                datasets_base_path / 'AnatEM-IOBES/test.tsv'
            ),
            'BC2GM': (
                datasets_base_path / 'BC2GM-IOBES/train.tsv',
                datasets_base_path / 'BC2GM-IOBES/devel.tsv',
                datasets_base_path / 'BC2GM-IOBES/test.tsv'
            ),
            'BC4CHEMD': (
                datasets_base_path / 'BC4CHEMD-IOBES/train.tsv',
                datasets_base_path / 'BC4CHEMD-IOBES/devel.tsv',
                datasets_base_path / 'BC4CHEMD-IOBES/test.tsv'
            ),
            'BC5CDR': (
                datasets_base_path / 'BC5CDR-IOBES/train.tsv',
                datasets_base_path / 'BC5CDR-IOBES/devel.tsv',
                datasets_base_path / 'BC5CDR-IOBES/test.tsv'
            ),
            'BioNLP09': (
                datasets_base_path / 'BioNLP09-IOBES/train.tsv',
                datasets_base_path / 'BioNLP09-IOBES/devel.tsv',
                datasets_base_path / 'BioNLP09-IOBES/test.tsv'
            ),
            'BioNLP11EPI': (
                datasets_base_path / 'BioNLP11EPI-IOBES/train.tsv',
                datasets_base_path / 'BioNLP11EPI-IOBES/devel.tsv',
                datasets_base_path / 'BioNLP11EPI-IOBES/test.tsv'
            ),
            'BioNLP11ID': (
                datasets_base_path / 'BioNLP11ID-IOBES/train.tsv',
                datasets_base_path / 'BioNLP11ID-IOBES/devel.tsv',
                datasets_base_path / 'BioNLP11ID-IOBES/test.tsv'
            ),
            'BioNLP13CG': (
                datasets_base_path / 'BioNLP13CG-IOBES/train.tsv',
                datasets_base_path / 'BioNLP13CG-IOBES/devel.tsv',
                datasets_base_path / 'BioNLP13CG-IOBES/test.tsv'
            ),
            'BioNLP13GE': (
                datasets_base_path / 'BioNLP13GE-IOBES/train.tsv',
                datasets_base_path / 'BioNLP13GE-IOBES/devel.tsv',
                datasets_base_path / 'BioNLP13GE-IOBES/test.tsv'
            ),
            'BioNLP13PC': (
                datasets_base_path / 'BioNLP13PC-IOBES/train.tsv',
                datasets_base_path / 'BioNLP13PC-IOBES/devel.tsv',
                datasets_base_path / 'BioNLP13PC-IOBES/test.tsv'
            ),
            'CRAFT': (
                datasets_base_path / 'CRAFT-IOBES/train.tsv',
                datasets_base_path / 'CRAFT-IOBES/devel.tsv',
                datasets_base_path / 'CRAFT-IOBES/test.tsv'
            ),
            'Ex-PTM': (
                datasets_base_path / 'Ex-PTM-IOBES/train.tsv',
                datasets_base_path / 'Ex-PTM-IOBES/devel.tsv',
                datasets_base_path / 'Ex-PTM-IOBES/test.tsv'
            ),
            'JNLPBA': (
                datasets_base_path / 'JNLPBA-IOBES/train.tsv',
                datasets_base_path / 'JNLPBA-IOBES/devel.tsv',
                datasets_base_path / 'JNLPBA-IOBES/test.tsv'
            ),
            'linnaeus': (
                datasets_base_path / 'linnaeus-IOBES/train.tsv',
                datasets_base_path / 'linnaeus-IOBES/devel.tsv',
                datasets_base_path / 'linnaeus-IOBES/test.tsv'
            ),
            'NCBI-disease': (
                datasets_base_path / 'NCBI-disease-IOBES/train.tsv',
                datasets_base_path / 'NCBI-disease-IOBES/devel.tsv',
                datasets_base_path / 'NCBI-disease-IOBES/test.tsv'
            )
        },

        'LM': {}
    }

    # *** Architecture ***

    # dimension of word embeddings
    wemb_dim = 200
    # dimension of character embeddings
    cemb_dim = 100

    # dimension of word-level LSTM
    word_lstm_dim = 300  # lstm on word embeddings
    # dimension of character-level LSTM
    char_lstm_dim = 100  # lstm on chars

    # type of character layer: 'bilstm' (BiLSTM) or 'conv' (convolution)
    char_layer_type = 'conv'
    filters = 50
    kernel_size = 3

    # *** Training ***

    # if False, word embeddings are randomly initialized
    use_pretrained_wembs = True
    # if True, pre-trained word embeddings will be adapted during training
    trainable_wembs = False

    # downsample all datasets to equal size?
    shrink_to_smallest = False

    batch_size = 20

    dropout = 0.5

    # optimizer: adam|adagrad|sgd|sgd-momentum|rmsprop
    lr_method = 'adam'
    sgd_momentum = 0.9

    # clipping if positive
    clip = -1

    # learning rate
    lr = 0.001
    lr_decay = 0.9

    epochs = 15
    # if None, complete dataset is iterated
    max_iter = None
    patience = 3

    def __init__(self, *overrides):
        """Initialize config values.

        Args:
            *overrides (str or object): Each argument is
                used to override the defaults.
                If it is a path (str), an attempt is made
                to import it as a Python module and get the
                name `Config`.
                Otherwise, any object is accepted.
                In both cases, it is checked for matching
                attributes for overriding.
        """
        self.timestamp = time.strftime('%Y%m%d-%H%M%S')
        report_filename = 'report_{}.txt'.format(self.timestamp)
        self.report_path = self.reports_path / report_filename

        # override config values
        for extra in overrides:
            # if module path
            if isinstance(extra, str):
                # load `Config` class from module
                extra = self._load_extra_config(extra)
            self._override(extra)

        self.data_path.mkdir(exist_ok=True)

        self.data_path = self.data_path / self._get_build_token()

    def _get_build_token(self):
        """Get a hash token from all build-related settings.

        Returns:
            str: The hash token.
        """
        h = hashlib.sha1()

        def _add(obj):
            h.update(repr(obj).encode('utf8'))

        _add(self.raw_wembs_path)
        _add(sorted(self.datasets_paths.items()))

        # Modify this message whenever the code changes in a way that
        # invalidates previous builds.
        _add('reserve vocab index 0 for padding')

        return h.hexdigest()

    @staticmethod
    def _load_extra_config(path):
        """Load `Config` class of a module.

        Args:
            path (str): Path to module with a class `Config`.

        Returns:
            extra.Config: The `Config` class.
        """
        from importlib.machinery import SourceFileLoader
        m = SourceFileLoader('extra', path).load_module()
        return m.Config

    def _override(self, extra):
        """Override the config values by those of another config `extra`.

        Args:
            extra (extra.Config): The `Config` class.
        """
        for name in self._params():
            if hasattr(extra, name):
                setattr(self, name, getattr(extra, name))

    def dump(self):
        """Serialise this config to a string.

        Returns:
            str: The serialised config.
        """
        template = '    {} = {!r}\n'
        representation = 'class Config:\n'
        for name in self._params():
            value = getattr(self, name)  # consider instance attributes too
            representation += template.format(name, value)
        return representation

    @classmethod
    def _params(cls):
        """Iterate over parameter names (all configurable settings).

        Yields:
            str: A parameter name.
        """
        for name in cls.__dict__:
            if name.startswith('_') or name in ('load', 'dump'):
                continue
            yield name
