import config as cfg


class Config(cfg.Config):

    model_path = cfg.Config.data_path / 'NER-BiLSTM' / 'weights'

    datasets_base_path = cfg.Config.data_path / 'data_bioner_5'

    raw_wembs_path = datasets_base_path / 'wikipedia-pubmed-and-PMC-w2v.txt'

    datasets_paths = {
        'NER': {
            'BC2GM': (
                datasets_base_path / 'BC2GM-IOBES/train.tsv',
                datasets_base_path / 'BC2GM-IOBES/devel.tsv',
                datasets_base_path / 'BC2GM-IOBES/test.tsv'
            ),
            'BC4CHEMD': (
                datasets_base_path / 'BC4CHEMD-IOBES/train.tsv',
                datasets_base_path / 'BC4CHEMD-IOBES/devel.tsv',
                datasets_base_path / 'BC4CHEMD-IOBES/test.tsv'
            ),
            'BC5CDR': (
                datasets_base_path / 'BC5CDR-IOBES/train.tsv',
                datasets_base_path / 'BC5CDR-IOBES/devel.tsv',
                datasets_base_path / 'BC5CDR-IOBES/test.tsv'
            ),
            'NCBI-disease': (
                datasets_base_path / 'NCBI-disease-IOBES/train.tsv',
                datasets_base_path / 'NCBI-disease-IOBES/devel.tsv',
                datasets_base_path / 'NCBI-disease-IOBES/test.tsv'
            ),
            'JNLPBA': (
                datasets_base_path / 'JNLPBA-IOBES/train.tsv',
                datasets_base_path / 'JNLPBA-IOBES/devel.tsv',
                datasets_base_path / 'JNLPBA-IOBES/test.tsv'
            )
        },

        'LM': {}
    }

    wemb_dim = 200
    cemb_dim = 30
    word_lstm_dim = 300
    char_lstm_dim = 300
    char_layer_type = 'bilstm'
    use_pretrained_wembs = True
    trainable_wembs = False
    dropout = 0.5
    lr = 0.01
    lr_decay = 0.9
    batch_size = 10
    epochs = 100
    lr_method = 'sgd-momentum'
    sgd_momentum = 0.9
    patience = 30
    clip = 5.0
