from pathlib import Path
import config


class Config(config.Config):

    datasets_base_path = Path('tests/datasets')

    datasets_paths = {
        'NER': {
            'BC5CDR': (
                datasets_base_path / 'BC5CDR/train.tsv',
                datasets_base_path / 'BC5CDR/devel.tsv',
                datasets_base_path / 'BC5CDR/test.tsv'
            ),
            'NCBI': (
                datasets_base_path / 'NCBI/train.tsv',
                datasets_base_path / 'NCBI/devel.tsv',
                datasets_base_path / 'NCBI/test.tsv'
            )
        },

        'LM': {}
    }

    use_pretrained_wembs = False
    trainable_wembs = True

    # Architecture
    char_layer_type = 'bilstm'
    char_lstm_dim = 20
    word_lstm_dim = 50
    cemb_dim = 10
    dropout = 0.5

    # Training
    batch_size = 10
    epochs = 2
