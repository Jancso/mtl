from configs.ner_bilstm_config import Config as Cfg
from util import create_lm_dataset


class Config(Cfg):

    model_path = Cfg.data_path / 'NER-LM-BiLSTM' / 'weights'

    create_lm_dataset.main(Cfg)

    Cfg.datasets_paths['LM']['BioLM'] = (
            Cfg.data_path / 'LM/train.tsv',
            Cfg.data_path / 'LM/dev.tsv',
            Cfg.data_path / 'LM/test.tsv',
    )
