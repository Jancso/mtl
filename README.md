# MTL

Multi-Task Learning (MTL) in Neural Networks for 
biomedical Named Entity Recognition (NER).

### Requirements

Python 3.6+

Dependencies:
* tensorflow>=1.0
* numpy
* progressbar
* blessings
* six
* gensim


Install dependencies via pip:
```
$ pip install -r requirements.txt
```

### Resources
* [Biomedical NER datasets](https://github.com/cambridgeltl/MTL-Bioinformatics-2016)
* [Biomedical Word Embeddings](http://bio.nlplab.org/)

### Architecture

![MTL diagram](figures/MTL-diagram.jpg)

### Usage
Set all paths and hyper-parameters in a config file (see `config.py`). 
Example configs can be find in `configs/`.
 
To train the model, run `mtl.py` passing the config as an argument.
```
$ python mtl.py configs/test_config.py
```

To evaluate the model, set the `train` parameter to `False` when invoking the
`main` method of `mtl.py`.
