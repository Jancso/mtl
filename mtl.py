import os
import sys
import shutil
from six.moves import reduce

import tensorflow as tf

from config import Config
from util.data_utils import *
from util.conlleval import evaluate, report as conll_report, metrics, \
    EvalCounts
from util.report import Reporter
from util.multiprogbar import ProgressBarGroup, ProgressBarGroupDummy
from util.build_data import build


class DataSet:

    def __init__(self):
        self.name = None
        self.task_type = None

        # data
        self.train = None
        self.dev = None
        self.test = None

        # labels
        self._label_vocab = None
        self.label2lid = None
        self.lid2label = None
        self.nlabels = None

        # tensors and ops
        self.logits_t = None
        self.labels_t = None
        self.trans_params_t = None
        self.loss_t = None
        self.lr_t = None
        self.train_op = None

    @property
    def label_vocab(self):
        return self._label_vocab

    @label_vocab.setter
    def label_vocab(self, value):
        """Set `_label_vocab` to `value`.

        In addition, create two callables that map a label to its ID and
        vice versa (`label2lid` and `lid2label`, respectively), and set
        the vocabulary size (`nlabels`).

        Args:
            value (Set[str]):  The label vocabulary.
        """
        self._label_vocab = value
        self.label2lid = label2lid(self._label_vocab)

        lid2label_dict = {self.label2lid(label): label
                          for label in self._label_vocab}

        def f(lid):
            return lid2label_dict[lid]

        self.lid2label = f

        self.nlabels = len(self.label_vocab) + 1  # +1: 0 reserved for padding


class MTLModel:
    """A multi-task learning neural network model.

    Attributes:
        config (Config): A `Config` instance for reading configs.
        reporter (Reporter): A `Reporter` instance for creating reports.
        datasets (List[DataSet]): Datasets to be processed.
        sess (tf.Session): Session in which graph will be launched.
        saver (tf.Saver): For saving and restoring model.
        wembs (numpy.array): Pre-trained word embeddings.
        nwords (int): Number of words in the vocabulary.
        word2wid (Callable[[str], int]): Returns the ID of a word.
        nchars (int): Number of characters in the vocabulary.
        char2cid (Callable[[str], List[int]]): Returns the IDs of the
            characters in the word.
    """

    def __init__(self):
        """Initialize variables."""
        self.config = None
        self.reporter = None

        self.datasets = []

        self.sess = None
        self.saver = None

        self.wembs = None

        self.wids_t = None
        self.cids_t = None
        self.slens_t = None
        self.wlens_t = None
        self.dropout_t = None

        self.nwords = None
        self.word2wid = None

        self.nchars = None
        self.char2cid = None

    def build(self, *configs):
        """Build the neural network model given the configs.

        Args:
            *configs (list[str|object]): Configs.
        """
        self.load_data(*configs)
        self.construct_graphs()

    def load_data(self, *configs):
        """Load the data.

        - Performs a data build if not done so yet
        - Loads the vocabularies and the word embeddings from this data build

        Args:
            *configs (list[str|object]): Configs.
        """
        # load configs
        self.config = Config(*configs)
        # set logging configs
        logging.basicConfig(format=self.config.logging_format,
                            level=self.config.logging_level)
        # create reporter
        self.reporter = Reporter(self.config)
        # build vocabularies and embeddings if not done so yet
        build(self.config)

        data_path = self.config.data_path

        # get pre-trained word embeddings
        if self.config.use_pretrained_wembs:
            self.wembs = get_embeddings_from_npz_file(data_path / 'wembs.npz')

        # get word vocabulary
        word_vocab = get_vocab_from_file(data_path / 'word_vocab.txt')
        self.word2wid = word2wid(word_vocab)
        self.nwords = len(word_vocab) + 1  # +1: 0 reserved for padding

        # get char vocabulary
        char_vocab = get_vocab_from_file(data_path / 'char_vocab.txt')
        self.char2cid = char2cid(char_vocab)
        self.nchars = len(char_vocab) + 1  # +1: 0 reserved for padding

        # collect datasets
        datasets_paths = self.config.datasets_paths
        for task_type in datasets_paths:
            for dataset_name, paths in datasets_paths[task_type].items():
                train_path, dev_path, test_path = paths

                dataset = DataSet()
                dataset.name = dataset_name
                dataset.task_type = task_type
                dataset.train = get_data_from_conll_files(train_path)
                dataset.dev = get_data_from_conll_files(dev_path)
                dataset.test = get_data_from_conll_files(test_path)

                label_vocab_path = data_path / dataset_name / 'label_vocab.txt'
                label_vocab = get_vocab_from_file(label_vocab_path)
                dataset.label_vocab = label_vocab

                self.datasets.append(dataset)

    def construct_graphs(self):
        """Construct the computational graph.

        First constructs the graph that is shared across all datasets, then
        constructs the dataset-specific graphs.
        """
        w_concat_fw_bw_t = self.get_shared_graph()

        for dataset in self.datasets:

            tensors = self.get_separate_graph(
                w_concat_fw_bw_t, dataset.nlabels,
                name=dataset.name, task_type=dataset.task_type)

            dataset.logits_t = tensors[0]
            dataset.labels_t = tensors[1]
            dataset.trans_params_t = tensors[2]
            dataset.loss_t = tensors[3]
            dataset.lr_t = tensors[4]
            dataset.train_op = tensors[5]

    def get_shared_graph(self):
        # shape = (batch size, max sent len in batch)
        self.wids_t = tf.placeholder(tf.int32, shape=[None, None], name="wids")
        # shape = (batch size, max sent len in batch, max word len in batch)
        self.cids_t = tf.placeholder(
            tf.int32, shape=[None, None, None], name="cids")
        # sent lens (without padding), shape = (batch size)
        self.slens_t = tf.placeholder(tf.int32, shape=[None], name="slens")
        # word lens (without padding),
        # shape = (batch size, max sent len in batch)
        self.wlens_t = tf.placeholder(
            tf.int32, shape=[None, None], name="wlens")
        # dropout
        self.dropout_t = tf.placeholder(
            dtype=tf.float32, shape=[], name="dropout")

        # use pre-trained word embeddings or not
        if self.wembs is None:
            # word embeddings matrix with randomly initialized weights
            # shape = (#words, wemb dim)
            wemb_matrix_t = tf.get_variable(
                name="wemb_matrix", dtype=tf.float32,
                shape=[self.nwords, self.config.wemb_dim])
        else:
            # word embeddings matrix with pre-trained weights
            # trainable: embeddings are continued to be trained or not
            wemb_matrix_t = tf.Variable(
                self.wembs, name="wemb_matrix", dtype=tf.float32,
                trainable=self.config.trainable_wembs)

        # look up word IDs, shape = (batch size, max sent len, wemb dim)
        wembs_t = tf.nn.embedding_lookup(
            wemb_matrix_t, self.wids_t, name="wembs")

        # embeddings matrix, shape: (#characters, cemb dim)
        cemb_matrix_t = tf.get_variable(
            name="cemb_matrix", dtype=tf.float32,
            shape=[self.nchars, self.config.cemb_dim])
        # look up IDs,
        # shape = (batch size, max sent len, max word len, cemb dim)
        cembs_t = tf.nn.embedding_lookup(cemb_matrix_t, self.cids_t,
                                         name="cembs")

        if self.config.char_layer_type == 'bilstm':
            cembs_shape_t = tf.shape(cembs_t)
            batch_dim = cembs_shape_t[0]
            sent_dim = cembs_shape_t[1]
            word_dim = cembs_shape_t[2]

            # remove batch dim -> iter words (rather than sents in batch)
            # shape = (#words of all sents, max word len, cemb dim)
            cembs_no_batch_t = tf.reshape(
                cembs_t,
                shape=[batch_dim * sent_dim, word_dim, self.config.cemb_dim],
                name='cembs_no_batch')
            # shape = (total number of words of all sentences,)
            wlens_no_batch_t = tf.reshape(
                self.wlens_t, shape=[batch_dim * sent_dim],
                name='wlens_no_batch')

            # LSTM cells for character embeddings
            c_fw_cell_t = tf.contrib.rnn.LSTMCell(
                self.config.char_lstm_dim, name='c_fw_cell')
            c_bw_cell_t = tf.contrib.rnn.LSTMCell(
                self.config.char_lstm_dim, name='c_bw_cell')

            with tf.variable_scope('c_bilstm'):
                # run BiLSTM through character embeddings
                c_bilstm_t = tf.nn.bidirectional_dynamic_rnn(
                    c_fw_cell_t, c_bw_cell_t, cembs_no_batch_t,
                    sequence_length=wlens_no_batch_t, dtype=tf.float32)

            # get the forward and the backward final states
            # shape = (number of words, char embedding dim)
            _, ((_, c_fw_final_state_t), (_, c_bw_final_state_t)) = c_bilstm_t

            # concatenate forward & backward final states
            # -> concatenate along the char embedding dimension
            # shape = (max sent len, fw cemb dim + bw cemb dim)
            c_concat_fw_bw_t = tf.concat(
                [c_fw_final_state_t, c_bw_final_state_t],
                axis=-1, name='c_concat_fw_bw')

            # add batch dimension back
            # shape = (batch size, max sent len, char hidden size)
            c_concat_fw_bw_with_batch_t = tf.reshape(
                c_concat_fw_bw_t,
                shape=[batch_dim, sent_dim, 2 * self.config.char_lstm_dim],
                name='c_concat_fw_bw_with_batch')

            # concatenate word and character embeddings
            # shape = (batch size, max sent len, word + char embedding dim)
            word_rep_t = tf.concat(
                [wembs_t, c_concat_fw_bw_with_batch_t], axis=-1,
                name='word_rep')
        else:
            # add dimension where all elements up to position wlen get the
            # Boolean value True and the rest of the elements False
            # shape = (batch size, max sent len, max word len)
            wlens_mask_t = tf.sequence_mask(self.wlens_t, name='wlens_mask')
            filters = self.config.filters
            kernel_size = self.config.kernel_size

            # get shape of char embeddings
            cembs_shape_t = tf.shape(cembs_t, name='cembs_shape')
            # get number of dimensions of char embeddings (-> 4)
            ndims = cembs_t.shape.ndims
            # get the dimensions except the last two and multiply them
            # (-> multiply batch size and max sent len dimensions)
            dim1_t = reduce(lambda x, y: x * y,
                            [cembs_shape_t[i] for i in range(ndims - 2)])
            # get second last dimension (-> dimension of max word len)
            dim2_t = cembs_shape_t[-2]
            # get last dimension (-> dimension of char embedding)
            dim3 = cembs_t.shape[-1]

            # remove batch size dimension, add dimension for each Boolean value
            # shape = (total number of words, max word len, 1)
            wlens_mask_no_batch_t = tf.reshape(
                wlens_mask_t, shape=[dim1_t, dim2_t, 1],
                name='wlens_mask_no_batch')

            # converts Boolean values to floats
            wlens_mask_no_batch_floats_t = tf.to_float(
                wlens_mask_no_batch_t, name='wlens_mask_no_batch_floats')

            # total number of words, max word len, char embedding
            flat_shape = [dim1_t, dim2_t, dim3]
            # remove batch size dimension
            # shape = (total number of words, max word len, char embedding)
            cembs_no_batch_t = tf.reshape(
                cembs_t, shape=flat_shape, name='cembs_no_batch')
            # multiply cembs by the mask (make padding zero)
            cembs_no_batch_t *= wlens_mask_no_batch_floats_t

            # apply convolution to char embeddings
            # shape = (total number of words, max word len, #filters)
            cembs_no_batch_conv_t = tf.layers.conv1d(
                cembs_no_batch_t, filters, kernel_size,
                padding='same', name='cembs_no_batch_conv')

            # make padding zero
            cembs_no_batch_conv_t *= wlens_mask_no_batch_floats_t

            # flip all zeros and ones: padding gets 1, rest 0
            flipped_mask_t = (1. - wlens_mask_no_batch_floats_t)

            # What does this do? Why is this necessary?
            # shape = (total number of words, max word len, #filters)
            cembs_no_batch_conv_t += flipped_mask_t * tf.reduce_min(
                cembs_no_batch_conv_t, axis=-2, keepdims=True)

            # for each filter across all chars, choose the highest filter
            # value -> word represented by the highest value of each filter
            # shape = (total number of words, #filters)
            cembs_no_batch_conv_max_t = tf.reduce_max(
                cembs_no_batch_conv_t, axis=-2, name='cembs_no_batch_conv_max')

            # add batch dimension back
            # shape = (batch size, max sent len, #filters)
            final_shape = ([cembs_shape_t[i] for i in range(ndims - 2)]) \
                + [filters]
            cembs_conv_max_t = tf.reshape(
                cembs_no_batch_conv_max_t, shape=final_shape,
                name='cembs_conv_max')

            # concatenate word embeddings + char-level word representation
            # shape = (batch size, max sent len, wembs + #filters)
            word_rep_t = tf.concat(
                [wembs_t, cembs_conv_max_t], axis=-1, name='word_rep')

        word_rep_t = tf.nn.dropout(word_rep_t, self.dropout_t,
                                   name='word_rep_dropout')

        # LSTM cells for word embeddings
        w_fw_cell_t = tf.contrib.rnn.LSTMCell(
            self.config.word_lstm_dim, name='w_fw_cell')
        w_bw_cell_t = tf.contrib.rnn.LSTMCell(
            self.config.word_lstm_dim, name='w_bw_cell')
        # run BiLSTM on word embeddings
        with tf.variable_scope('w_bilstm'):
            w_bilstm_t = tf.nn.bidirectional_dynamic_rnn(
                w_fw_cell_t, w_bw_cell_t, word_rep_t,
                sequence_length=self.slens_t, dtype=tf.float32)
        # get forward & backward hidden states of each timestep
        (w_fw_hidden_t, w_bw_hidden_t), _ = w_bilstm_t
        # concatenate the hidden states, shape = (batch size, max sent len,
        # 2*dim of hidden LSTM layer)
        w_concat_fw_bw_t = tf.concat(
            [w_fw_hidden_t, w_bw_hidden_t], axis=-1, name='w_concat_fw_bw')
        w_concat_fw_bw_t = tf.nn.dropout(
            w_concat_fw_bw_t, self.dropout_t, name='w_concat_fw_bw_dropout')

        return w_concat_fw_bw_t

    def get_separate_graph(
            self, hidden_t, nlabels, name='Dataset', task_type='NER'):

        with tf.variable_scope(name):
            # shape = (2*dimension of hidden LSTM layer, number of tags)
            w_t = tf.get_variable(
                "W", dtype=tf.float32,
                shape=[2 * self.config.word_lstm_dim, nlabels])
            # shape = (number of tags)
            bias_t = tf.get_variable(
                "b", shape=[nlabels], dtype=tf.float32,
                initializer=tf.zeros_initializer())

            # number of words (max sentence length) per sentence
            nsteps_t = tf.shape(hidden_t, name='nsteps')[1]

            # shape = (total number of words, 2*dimension of hidden LSTM layer)
            # (-1 means shape is inferred)
            reshp_bilstm_states_t = tf.reshape(
                hidden_t, [-1, 2 * self.config.word_lstm_dim],
                name='reshp_bilstm_states')
            # projection onto score labels
            # shape = (total number of words, number of labels)
            pred_t = tf.matmul(reshp_bilstm_states_t, w_t,
                               name='matmul') + bias_t
            # shape = (batch size, max sentence length, number of labels)
            logits_t = tf.reshape(
                pred_t, [-1, nsteps_t, nlabels], name='logits')

            # shape = (batch size, max length of sentence)
            labels_t = tf.placeholder(
                tf.int32, shape=[None, None], name="labels")

            # compute loss
            if task_type == 'LM':
                # not needed
                trans_params_t = None

                # compute loss for each word
                # shape = (batch size, max sent len)
                losses = tf.nn.sparse_softmax_cross_entropy_with_logits(
                    logits=logits_t, labels=labels_t, name='losses')
                # apply mask to ignore padding
                mask = tf.sequence_mask(self.slens_t)
                # shape = (total number of words,)
                masked_losses = tf.boolean_mask(
                    losses, mask, name='masked_losses')
                loss_t = tf.reduce_sum(masked_losses)

            else:
                with tf.variable_scope('crf'):
                    # get the log-likelihood of tag sequences in a CRF
                    log_likelihood_t, trans_params_t = \
                        tf.contrib.crf.crf_log_likelihood(
                            logits_t, labels_t, self.slens_t)
                loss_t = tf.reduce_mean(-log_likelihood_t, name='loss')

            # learning rate
            lr_t = tf.placeholder(dtype=tf.float32, shape=[], name="lr")

            # optimizer
            lr_method = self.config.lr_method.lower()

            if lr_method == 'adam':
                optimizer_t = tf.train.AdamOptimizer(lr_t, name='optimizer')
            elif lr_method == 'adagrad':
                optimizer_t = tf.train.AdagradOptimizer(lr_t, name='optimizer')
            elif lr_method == 'sgd':
                optimizer_t = tf.train.GradientDescentOptimizer(
                    lr_t, name='optimizer')
            elif lr_method == 'sgd-momentum':
                optimizer_t = tf.train.MomentumOptimizer(
                    lr_t, self.config.sgd_momentum, name='optimizer')
            elif lr_method == 'rmsprop':
                optimizer_t = tf.train.RMSPropOptimizer(lr_t, name='optimizer')
            else:
                raise NotImplementedError(
                    "Unknown method {}".format(lr_method))

            # clip gradients if positive
            if self.config.clip > 0:
                # compute gradients
                grads, variables = zip(*optimizer_t.compute_gradients(loss_t))
                # clip gradients
                grads, _ = tf.clip_by_global_norm(grads, self.config.clip)
                # apply gradients
                train_op = optimizer_t.apply_gradients(zip(grads, variables),
                                                       name='train_op')
            else:
                # compute + apply gradients
                train_op = optimizer_t.minimize(loss_t, name='train_op')

            return logits_t, labels_t, trans_params_t, loss_t, lr_t, train_op

    def initialize_session(self, restore=True):
        cp = tf.ConfigProto(inter_op_parallelism_threads=1)
        self.sess = tf.Session(config=cp)
        # self.sess = tf_debug.TensorBoardDebugWrapperSession(
        #                   sess, "UX360UAK:6064")

        # for saving and restoring models
        self.saver = tf.train.Saver()

        model_path = self.config.model_path

        # whether a model should be restored or newly created
        if not model_path.is_dir():
            model_path.mkdir(parents=True)
            self.sess.run(tf.global_variables_initializer())
        else:
            if restore:
                self.saver.restore(self.sess, str(model_path))
            else:
                answer = input(
                    'Model found at {}! '
                    'Do you really want to delete it? '
                    'yes/no: '.format(model_path))
                if answer == 'yes':
                    shutil.rmtree(model_path.parent)
                    model_path.mkdir(parents=True)
                    self.sess.run(tf.global_variables_initializer())
                else:
                    self.saver.restore(self.sess, str(model_path))

        self.sess.graph.finalize()

    def train(self):
        sampler = ShuffledSampler([d.train for d in self.datasets],
                                  self.config.batch_size,
                                  self.config.shrink_to_smallest)

        if logging.getLogger().getEffectiveLevel() > logging.INFO:
            pbargroup = ProgressBarGroupDummy
        else:
            pbargroup = ProgressBarGroup

        nepochs = self.config.epochs
        # tensorflow does not support pathlib.Path yet
        model_path = str(self.config.model_path)
        lr = self.config.lr
        dropout = self.config.dropout
        # decay after each epoch
        lr_decay = self.config.lr_decay
        best_score = 0
        patience = self.config.patience
        nepoch_no_imprv = 0

        self.saver.save(self.sess, model_path)

        for epoch in range(nepochs):

            logging.info("Epoch %d out of %d", epoch + 1, nepochs)

            pbars = pbargroup()
            for dataset, nbatches in zip(self.datasets, sampler.nbatches()):
                pbars.add(dataset.name, nbatches, 'loss: {:g}   ')

            # train model on all batches of all datasets
            for i, minibatch in sampler:
                dataset = self.datasets[i]
                progbar = pbars.pbars[i]

                cids, wids, lids, slens, wlens = extract_data(
                    minibatch, self.char2cid, self.word2wid, dataset.label2lid)

                feed_dict = {
                    self.cids_t: cids,
                    self.wlens_t: wlens,
                    self.wids_t: wids,
                    self.slens_t: slens,
                    self.dropout_t: dropout,
                    dataset.labels_t: lids,
                    dataset.lr_t: lr
                }

                _, loss = self.sess.run(
                    [dataset.train_op, dataset.loss_t], feed_dict=feed_dict)
                progbar.update(loss)

            pbars.finish()

            overall_scores, scores_by_dataset = self._evaluate('dev')

            # early stopping
            if overall_scores.fscore > best_score:
                logging.info('New best F1-score - saving model!')
                # save current model
                self.saver.save(self.sess, model_path)
                self.reporter.add_results(overall_scores, scores_by_dataset)
                nepoch_no_imprv = 0
                best_score = overall_scores.fscore
            else:
                nepoch_no_imprv += 1
                if nepoch_no_imprv >= patience:
                    logging.info(
                        "No improvement in F1-score - early stopping!")
                    break

            # decay learning rate
            lr *= lr_decay

    def evaluate(self, mode='test'):
        """Evaluate model on a test set.

        Args:
            mode (str): 'dev' or 'test' for tuning or testing, respectively
        """
        self.reporter.add_results(*self._evaluate(mode))

    def _evaluate(self, mode):
        if logging.getLogger().getEffectiveLevel() > logging.INFO:
            eval_out = open(os.devnull, 'w')
        else:
            eval_out = sys.stdout

        # for NER
        scores_by_dataset = {}
        overall_counts = EvalCounts()
        # for LM
        overall_loss = 0.0
        overall_n_words = 0

        batch_size = self.config.batch_size

        for dataset in self.datasets:
            # for NER
            predictions = []
            # for LM
            dataset_loss = 0.0
            dataset_n_words = 0

            if mode == 'dev':
                test_set = dataset.dev
            else:
                test_set = dataset.test

            for minibatch in iter_in_minibatches(test_set, batch_size):

                cids, wids, lids, slens, wlens = extract_data(
                    minibatch, self.char2cid, self.word2wid, dataset.label2lid)

                feed_dict = {
                    self.cids_t: cids,
                    self.wlens_t: wlens,
                    self.wids_t: wids,
                    self.slens_t: slens,
                    self.dropout_t: 1.0,
                    dataset.labels_t: lids
                }

                if dataset.task_type == 'LM':
                    loss = self.sess.run(dataset.loss_t, feed_dict=feed_dict)
                    dataset_loss += loss
                    dataset_n_words += sum(slens)
                else:
                    # get tag scores and transition params of CRF
                    logits, trans_params = self.sess.run(
                        [dataset.logits_t, dataset.trans_params_t],
                        feed_dict=feed_dict)

                    # iterate over sentences -> labels, logits and lengths
                    for slids, slogits, slen in zip(lids, logits, slens):
                        # ignore the padding
                        slids = slids[:slen]
                        slogits = slogits[:slen]
                        slids_pred, viterbi_score = \
                            tf.contrib.crf.viterbi_decode(
                                slogits, trans_params)

                        for lid, lid_pred in zip(slids, slids_pred):
                            label = dataset.lid2label(lid)
                            label_pred = dataset.lid2label(lid_pred)
                            predictions.append(
                                "\t".join(["dummy", label, label_pred]))

            # report dataset-wise scores
            if dataset.task_type == 'LM':
                print(dataset.name, file=eval_out)
                overall_loss += dataset_loss
                overall_n_words += dataset_n_words
                perplexity = np.exp(dataset_loss / dataset_n_words)
                logging.info(
                    '%s-set perplexity for %s: %g', mode,
                    dataset.name, perplexity)
            else:
                print(dataset.name, file=eval_out)
                counts = evaluate(predictions)
                conll_report(counts, out=eval_out)
                scores_by_dataset[dataset.name] = scores = metrics(counts)[0]
                overall_counts += counts
                logging.info('%s-set F1-score for %s: %g',
                             mode, dataset.name, scores.fscore)

        # report overall scores
        print('Overall', file=eval_out)
        # LM report: only log if there was a LM dataset
        if overall_n_words != 0:
            logging.info('Overall %s-set perplexity: %g',
                         mode, np.exp(overall_loss/overall_n_words))
        # NER report
        conll_report(overall_counts, out=eval_out)
        overall_scores = metrics(overall_counts)[0]
        logging.info('Overall %s-set F1-score: %g',
                     mode, overall_scores.fscore)

        return overall_scores, scores_by_dataset

    def report(self):
        """Write a report to disk."""
        self.reporter.dump()


def main(*args, train=True, restore=True):
    """Train or evaluate a model.

    Note:
        Training and evaluating in the same process doesn't work.

    Args:
        *args (list): Command-line arguments.
        train (bool): Train (True) or evaluate (False)
        restore (bool): Restore (True) or create (False) new session.
    """
    model = MTLModel()
    model.build(*args)
    model.initialize_session(restore=restore)

    try:
        if train:
            model.train()
        else:
            model.evaluate()
    except Exception as e:
        logging.exception('interrupted with %r', e)
        raise
    finally:
        model.report()


if __name__ == '__main__':
    main(*sys.argv[1:], train=True, restore=True)
